# CDGVP

Control de Guardias de seguridad para la empresa VIPRISEG. (Manejo de Recursos Humanos)

## Name
Control de Guardias VIPRISEG (CDGVP)


## Description
Gestor de talento humano de la empresa de seguridad de guardias VIPRISEG. 
Automatiza el proceso de contratación del personal de guardias, administrando toda su información de ingreso, especialmente su disponibilidad para seguir o no en el empresa.
Controla los recursos disponible(guardias), que estén en condiciones para ejercer el servicio.


## Authors and acknowledgment

* Cristian Tontaquimba - cstontaquimbal@utn.edu.ec
* Ulises Valencia - uivalencial@utn.edu.ec
* Roberth Lima - rglimac@utn.edu.ec

-----------------------------------------------
*Estudiantes de la Universidad Técnica del Norte* 

## Installation

Proyecto tipo JAVA EE EAR, desarrollado en el entorno Eclipse IDE for Enterprise Java and Web developers. Version: 2022-03 
Servidor: wildfly 22.0.1

Establecer el datasource con el nombre CDGVP



## License
Uso exclusivo para la empresa VIPRISEG.
EL código puede ser usado como base para proyectos similares.


