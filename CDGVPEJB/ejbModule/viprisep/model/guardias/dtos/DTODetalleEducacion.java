package viprisep.model.guardias.dtos;

public class DTODetalleEducacion {
	private String nivel;
	private String carrera;
	private Boolean registrado;
	private String nombreEmpresa;
	private String tipo;
	private String categoria;
	private String provinciaCiudad;
	
	public String getNivel() {
		return nivel;
	}
	public void setNivel(String nivel) {
		this.nivel = nivel;
	}
	public String getCarrera() {
		return carrera;
	}
	public void setCarrera(String carrera) {
		this.carrera = carrera;
	}
	public Boolean getRegistrado() {
		return registrado;
	}
	public void setRegistrado(Boolean registrado) {
		this.registrado = registrado;
	}
	
	public String getNombreEmpresa() {
		return nombreEmpresa;
	}
	public void setNombreEmpresa(String nombreEmpresa) {
		this.nombreEmpresa = nombreEmpresa;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getCategoria() {
		return categoria;
	}
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	public String getProvinciaCiudad() {
		return provinciaCiudad;
	}
	public void setProvinciaCiudad(String provinciaCiudad) {
		this.provinciaCiudad = provinciaCiudad;
	}
	
	
}
