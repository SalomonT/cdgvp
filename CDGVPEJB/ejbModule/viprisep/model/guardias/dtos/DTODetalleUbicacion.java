package viprisep.model.guardias.dtos;

public class DTODetalleUbicacion {
	private String nombreCiudad;
	private int codigoProvincia;
	
	public String getNombreCiudad() {
		return nombreCiudad;
	}
	public void setNombreCiudad(String nombreCiudad) {
		this.nombreCiudad = nombreCiudad;
	}
	public int getcodigoProvincia() {
		return codigoProvincia;
	}
	public void setcodigoProvincia(int codigoProvincia) {
		this.codigoProvincia = codigoProvincia;
	}
	
}
