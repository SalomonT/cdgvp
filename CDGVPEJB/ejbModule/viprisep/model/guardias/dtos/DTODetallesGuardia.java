package viprisep.model.guardias.dtos;

public class DTODetallesGuardia {

	private String nombreEmpresa;
	private String tipo;
	private String Categoria;
	private String ciudadEmpresa;
	private String provinciaEmpresa;
	private int idEmpresa;

	public String getNombreEmpresa() {
		return nombreEmpresa;
	}

	public int getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(int idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public void setNombreEmpresa(String nombreEmpresa) {
		this.nombreEmpresa = nombreEmpresa;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getCategoria() {
		return Categoria;
	}

	public void setCategoria(String categoria) {
		Categoria = categoria;
	}

	public String getCiudadEmpresa() {
		return ciudadEmpresa;
	}

	public void setCiudadEmpresa(String ciudadEmpresa) {
		this.ciudadEmpresa = ciudadEmpresa;
	}

	public String getProvinciaEmpresa() {
		return provinciaEmpresa;
	}

	public void setProvinciaEmpresa(String provinciaEmpresa) {
		this.provinciaEmpresa = provinciaEmpresa;
	}

}
