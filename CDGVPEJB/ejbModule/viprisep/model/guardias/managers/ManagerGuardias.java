package viprisep.model.guardias.managers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.xml.soap.Detail;

import minimarketdemo.model.core.entities.CdcCategoria;
import minimarketdemo.model.core.entities.CdcClasificacion;
import minimarketdemo.model.core.entities.CdcEmpresaCabecera;
import minimarketdemo.model.core.entities.CdcEmpresaDetalle;
import minimarketdemo.model.core.entities.CdgCiudad;
import minimarketdemo.model.core.entities.CdgGuardiaCabecera;
import minimarketdemo.model.core.entities.CdgGuardiaDetalle;
import minimarketdemo.model.core.entities.CdgHistorial;
import minimarketdemo.model.core.entities.CdgNivelEducacion;
import minimarketdemo.model.core.entities.CdgProvincia;
import viprisep.model.guardias.dtos.DTODetalleEducacion;
import viprisep.model.guardias.dtos.DTODetallesEmpresas;
import viprisep.model.guardias.dtos.DTODetallesGuardia;

/**
 * Session Bean implementation class ManagerGuardias
 */
@Stateless
@LocalBean
public class ManagerGuardias {

	@PersistenceContext
	private EntityManager em;

	public ManagerGuardias() {
		// TODO Auto-generated constructor stub
	}

	public List<CdgNivelEducacion> findAllCdgNivelEducacion() {
		TypedQuery<CdgNivelEducacion> q = em.createQuery("SELECT c FROM CdgNivelEducacion c", CdgNivelEducacion.class);
		return q.getResultList();
	}

	public List<CdgGuardiaCabecera> findAllCdgGuardiaCabecera() {
		TypedQuery<CdgGuardiaCabecera> q = em.createQuery("SELECT c FROM CdgGuardiaCabecera c",
				CdgGuardiaCabecera.class);
		return q.getResultList();
	}

	public List<CdgGuardiaDetalle> findAllCdgGuardiaDetalle() {
		TypedQuery<CdgGuardiaDetalle> q = em.createQuery("SELECT c FROM CdgGuardiaDetalle c", CdgGuardiaDetalle.class);
		return q.getResultList();
	}

	public DTODetallesGuardia crearDetalleGuardia(int idEmpresaDetalle) {

		CdcEmpresaDetalle emp = em.find(CdcEmpresaDetalle.class, idEmpresaDetalle);

		DTODetallesGuardia detalle = new DTODetallesGuardia();
		detalle.setNombreEmpresa(emp.getCdcEmpresaCabecera().getNombreEmpresa());
		detalle.setTipo(emp.getTipo());
		detalle.setCategoria(emp.getCdcEmpresaCabecera().getCdcCategoria().getNombreCategoria());
		detalle.setProvinciaEmpresa(emp.getCdgCiudad().getCdgProvincia().getNombreProvincia());
		detalle.setCiudadEmpresa(emp.getCdgCiudad().getNombreCiudad());
		detalle.setIdEmpresa(idEmpresaDetalle);

		return detalle;
	}

	public void guardiasMultipleEmpresas(List<DTODetallesGuardia> listaDetallesGuardias, int idEmpresaDetalle,
			String idGuardiaCab) {

		CdgGuardiaCabecera cabecera = em.find(CdgGuardiaCabecera.class, idGuardiaCab);

		List<CdgGuardiaDetalle> listaDetalle = new ArrayList<CdgGuardiaDetalle>();
		cabecera.setCdgGuardiaDetalles(listaDetalle);

		for (DTODetallesGuardia det : listaDetallesGuardias) {
			CdgGuardiaDetalle malDet = new CdgGuardiaDetalle();
			malDet.setCdgGuardiaCabecera(cabecera);
			CdcEmpresaDetalle emp = em.find(CdcEmpresaDetalle.class, det.getIdEmpresa());
			malDet.setCdcEmpresaDetalle(emp);
			listaDetalle.add(malDet);
			em.merge(cabecera);
		}
	}

	/// Métodos para el historial
	public void insertarHistorial(String cedula, Date fechaInicio, Date fechaFin, Boolean estado) {
		CdgHistorial historial = new CdgHistorial();

		CdgGuardiaCabecera cabecera = new CdgGuardiaCabecera();
		cabecera = em.find(CdgGuardiaCabecera.class, cedula);

		historial.setCdgGuardiaCabecera(cabecera);
		historial.setFechaIngreso(fechaInicio);
		historial.setFechaSalida(fechaFin);
		historial.setEstado(estado);
		em.persist(historial);
		// JSFUtil.crearMensajeINFO("Ingregado al historial");
	}

	public List<CdgHistorial> findAllHistorial() {
		TypedQuery<CdgHistorial> q = em.createQuery("SELECT c FROM CdgHistorial c", CdgHistorial.class);
		return q.getResultList();
	}

	public void editarHistorial(int idHistorial, CdgHistorial historialEdit) throws Exception {
		CdgHistorial historial = em.find(CdgHistorial.class, idHistorial);
		historial.setCdgGuardiaCabecera(historialEdit.getCdgGuardiaCabecera());
		historial.setEstado(historialEdit.getEstado());
		historial.setFechaIngreso(historialEdit.getFechaIngreso());

		historial.setFechaSalida(historialEdit.getFechaSalida());

	}

}
