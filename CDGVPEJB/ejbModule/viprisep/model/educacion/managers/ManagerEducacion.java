package viprisep.model.educacion.managers;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import minimarketdemo.model.core.entities.CdgNivelEducacion;
import minimarketdemo.model.core.entities.CdgProvincia;

/**
 * Session Bean implementation class ManagerEducacion
 */
@Stateless
@LocalBean
public class ManagerEducacion {

	@PersistenceContext
	private EntityManager em;

	public ManagerEducacion() {
		// TODO Auto-generated constructor stub
	}

	public List<CdgNivelEducacion> findAllCdgNivelEducacion() {
		TypedQuery<CdgNivelEducacion> q = em.createQuery("SELECT c FROM CdgNivelEducacion c", CdgNivelEducacion.class);
		return q.getResultList();
	}

	public void actualizarEdu(CdgNivelEducacion eduEdicion) {
		CdgNivelEducacion a = em.find(CdgNivelEducacion.class, eduEdicion.getIdCdgNivelEducacion());
		a.setCarrera(eduEdicion.getCarrera());
		a.setRegistrado(eduEdicion.getRegistrado());
		em.merge(a);
	}

	public void EliminarEdu(Integer IdEdu) {
		CdgNivelEducacion fer = em.find(CdgNivelEducacion.class, IdEdu);
		em.remove(fer);
	}

	public void guardarEducacion(String Nivel, String Carrera, Boolean Registrado) {
		CdgNivelEducacion fer = new CdgNivelEducacion();
		fer.setNivel(Nivel);
		fer.setCarrera(Carrera);
		fer.setRegistrado(Registrado);
		em.persist(fer);
	}

}
