package viprisep.model.ubicacion.managers;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import minimarketdemo.model.core.entities.CdgCiudad;
import minimarketdemo.model.core.entities.CdgGuardiaCabecera;
import minimarketdemo.model.core.entities.CdgNivelEducacion;
import minimarketdemo.model.core.entities.CdgProvincia;
import viprisep.model.guardias.dtos.DTODetalleEducacion;
import viprisep.model.guardias.dtos.DTODetalleUbicacion;

/**
 * Session Bean implementation class ManagerUbicacion
 */
@Stateless
@LocalBean
public class ManagerUbicacion {

	@PersistenceContext
	private EntityManager em;

	public ManagerUbicacion() {
		// TODO Auto-generated constructor stub
	}

	public List<CdgProvincia> findAllCdgProvincia() {
		TypedQuery<CdgProvincia> q = em.createQuery("SELECT c FROM CdgProvincia c", CdgProvincia.class);
		return q.getResultList();
	}

	public List<CdgCiudad> findAllCdgCiudad() {
		TypedQuery<CdgCiudad> q = em.createQuery("SELECT c FROM CdgCiudad c", CdgCiudad.class);
		return q.getResultList();
	}

	/*
	 * public List<CdgCiudad> findProvinciaWithCities() { TypedQuery<CdgCiudad> q =
	 * em.createQuery("select o from CdgCiudad o where o.CdgProvincia. is not null",
	 * CdgCiudad.class); return q.getResultList(); }
	 */

	public DTODetalleUbicacion crearDetalleUbicacion(int idProvincia, String nombreCiudad) {
		DTODetalleUbicacion detalle = new DTODetalleUbicacion();
		detalle.setcodigoProvincia(idProvincia);
		detalle.setNombreCiudad(nombreCiudad);
		return detalle;
	}

	public void ubicacionMultipleGuardia(List<DTODetalleUbicacion> listaDetalleUbicacion, int idProvinciaSeleccionada,
			String nombreCiudad) {

		CdgProvincia provincita = new CdgProvincia();

		List<CdgCiudad> listaDetalle = new ArrayList<CdgCiudad>();
		CdgProvincia pro = em.find(CdgProvincia.class, idProvinciaSeleccionada);

		for (DTODetalleUbicacion det : listaDetalleUbicacion) {
			CdgCiudad malDet = new CdgCiudad();
			malDet.setCdgProvincia(pro);
			malDet.setNombreCiudad(det.getNombreCiudad());
			listaDetalle.add(malDet);
			em.persist(malDet);
		}
	}

	public void actualizarCiudad(CdgCiudad ciudad) {
		CdgCiudad a = em.find(CdgCiudad.class, ciudad.getIdCdgCiudad());
		a.setNombreCiudad(ciudad.getNombreCiudad());
		em.merge(a);
		
	}

	public void EliminarCiudad(int idCiudad) {
		CdgCiudad ciudadcita = em.find(CdgCiudad.class, idCiudad);
		if (ciudadcita != null) {
			em.remove(ciudadcita);
			System.out.println(ciudadcita);
		}
	}

	/*
	 * 
	 * public void EliminarCiudad(CdgCiudad ciudad) { CdgCiudad a =
	 * em.find(CdgCiudad.class, ciudad.getIdCdgCiudad()); em.remove(a); }
	 */
}
