package viprisep.model.parametros.managers;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import minimarketdemo.model.core.entities.CdcCategoria;
import minimarketdemo.model.core.entities.CdcClasificacion;
import minimarketdemo.model.core.entities.CdgNivelEducacion;

/**
 * Session Bean implementation class ManagerParametros
 */
@Stateless
@LocalBean
public class ManagerParametros {

	/**
	 * Default constructor.
	 */

	@PersistenceContext
	private EntityManager em;

	public ManagerParametros() {
		// TODO Auto-generated constructor stub
	}

	public void actualizarClasificacion(CdcClasificacion clasiEdicion) {
		CdcClasificacion clasi = em.find(CdcClasificacion.class, clasiEdicion.getIdCdgClasificacion());
		clasi.setNombreClasificacion(clasiEdicion.getNombreClasificacion());
		em.merge(clasi);
	}

	public void actualizarCategoria(CdcCategoria catoEdicion) {
		CdcCategoria cato = em.find(CdcCategoria.class, catoEdicion.getIdCdgCategoria());
		cato.setNombreCategoria(catoEdicion.getNombreCategoria());
		em.merge(cato);
	}

	public List<CdcCategoria> findAllCdcCategoria() {
		TypedQuery<CdcCategoria> q = em.createQuery("SELECT c FROM CdcCategoria c", CdcCategoria.class);
		return q.getResultList();
	}

	public List<CdcClasificacion> findAllCdcClasificacion() {
		TypedQuery<CdcClasificacion> q = em.createQuery("SELECT c FROM CdcClasificacion c", CdcClasificacion.class);
		return q.getResultList();
	}

	public void guardarClasificacion(String nombre) {
		CdcClasificacion clasi = new CdcClasificacion();
		clasi.setNombreClasificacion(nombre);
		em.persist(clasi);
	}

	public void EliminarClasificacion(Integer IdClasificacion) {
		CdcClasificacion clasi = em.find(CdcClasificacion.class, IdClasificacion);
		em.remove(clasi);
	}

	public void EliminarCategoria(Integer IdCategoria) {
		CdcCategoria cato = em.find(CdcCategoria.class, IdCategoria);
		em.remove(cato);
	}

	public void guardarCategoria(String nombre) {
		CdcCategoria cato = new CdcCategoria();
		cato.setNombreCategoria(nombre);
		em.persist(cato);
	}

}
