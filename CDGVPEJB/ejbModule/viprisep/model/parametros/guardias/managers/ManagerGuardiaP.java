package viprisep.model.parametros.guardias.managers;

import java.util.Date;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import minimarketdemo.model.core.entities.CdcCategoria;
import minimarketdemo.model.core.entities.CdcClasificacion;
import minimarketdemo.model.core.entities.CdgCiudad;
import minimarketdemo.model.core.entities.CdgGuardiaCabecera;
import minimarketdemo.model.core.entities.CdgNivelEducacion;

/**
 * Session Bean implementation class ManagerGuardiaP
 */
@Stateless
@LocalBean
public class ManagerGuardiaP {

	@PersistenceContext
	private EntityManager em;

	public ManagerGuardiaP() {
		// TODO Auto-generated constructor stub
	}

	public List<CdgNivelEducacion> findAllCdgNivelEducacion() {
		TypedQuery<CdgNivelEducacion> q = em.createQuery("SELECT c FROM CdgNivelEducacion c", CdgNivelEducacion.class);
		return q.getResultList();
	}

	public List<CdgCiudad> findAllCdgCiudad() {
		TypedQuery<CdgCiudad> q = em.createQuery("SELECT c FROM CdgCiudad c", CdgCiudad.class);
		return q.getResultList();
	}

	public List<CdgGuardiaCabecera> findAllCdgGuardiaCabecera() {
		TypedQuery<CdgGuardiaCabecera> q = em.createQuery("SELECT c FROM CdgGuardiaCabecera c",
				CdgGuardiaCabecera.class);
		return q.getResultList();
	}

	public void guardarGuardia(String cedula, String Apellidos, String Nombres, Date fechaNacimiento, String Genero,
			String celular, Boolean reentrenado, Boolean discapacidad, int porcentajeDiscapacidad,
			int idTituloSeleccionado, int idUbicacionSeleccionada) {

		CdgGuardiaCabecera guardia = new CdgGuardiaCabecera();
		CdgNivelEducacion edu = em.find(CdgNivelEducacion.class, idTituloSeleccionado);
		CdgCiudad ubi = em.find(CdgCiudad.class, idUbicacionSeleccionada);

		guardia.setCedula(cedula);
		guardia.setApellidos(Apellidos);
		guardia.setNombres(Nombres);
		guardia.setFechaNacimiento(fechaNacimiento);
		guardia.setGenero(Genero);
		guardia.setCelular(celular);
		guardia.setReentrenado(reentrenado);
		guardia.setDiscapacidad(discapacidad);
		guardia.setPorcentajeDiscapacidad(porcentajeDiscapacidad);
		guardia.setCdgNivelEducacion(edu);
		guardia.setCdgCiudad(ubi);

		em.persist(guardia);
	}

	public void actualizarGuardiaCab(CdgGuardiaCabecera guardiacab, int idTituloSeleccionado,
			int idUbicacionSeleccionado) {

		CdgNivelEducacion edu = em.find(CdgNivelEducacion.class, idTituloSeleccionado);
		CdgCiudad siu = em.find(CdgCiudad.class, idUbicacionSeleccionado);

		CdgGuardiaCabecera guau = em.find(CdgGuardiaCabecera.class, guardiacab.getCedula());
		guau.setCedula(guardiacab.getCedula());
		guau.setApellidos(guardiacab.getApellidos());
		guau.setNombres(guardiacab.getNombres());
		guau.setFechaNacimiento(guardiacab.getFechaNacimiento());
		guau.setGenero(guardiacab.getGenero());
		guau.setCelular(guardiacab.getCelular());
		guau.setReentrenado(guardiacab.getReentrenado());
		guau.setDiscapacidad(guardiacab.getDiscapacidad());
		guau.setPorcentajeDiscapacidad(guardiacab.getPorcentajeDiscapacidad());
		guau.setCdgNivelEducacion(edu);
		guau.setCdgCiudad(siu);

		em.merge(guau);
	}

	public void EliminarGuardia(String IdGuardia) {
		CdgGuardiaCabecera guardia = em.find(CdgGuardiaCabecera.class, IdGuardia);
		em.remove(guardia);
	}

}
