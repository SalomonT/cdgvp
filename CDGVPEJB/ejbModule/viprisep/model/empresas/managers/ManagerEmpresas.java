package viprisep.model.empresas.managers;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import minimarketdemo.model.core.entities.CdcCategoria;
import minimarketdemo.model.core.entities.CdcClasificacion;
import minimarketdemo.model.core.entities.CdcEmpresaCabecera;
import minimarketdemo.model.core.entities.CdcEmpresaDetalle;
import minimarketdemo.model.core.entities.CdgCiudad;
import minimarketdemo.model.core.entities.CdgProvincia;
import viprisep.model.guardias.dtos.DTODetalleUbicacion;
import viprisep.model.guardias.dtos.DTODetallesEmpresas;

/**
 * Session Bean implementation class ManagerEmpresas
 */
@Stateless
@LocalBean
public class ManagerEmpresas {

	@PersistenceContext
	private EntityManager em;

	public ManagerEmpresas() {
		// TODO Auto-generated constructor stub
	}

	public List<CdcEmpresaCabecera> findAllCdcEmpresaCabecera() {
		TypedQuery<CdcEmpresaCabecera> q = em.createQuery("SELECT c FROM CdcEmpresaCabecera c",
				CdcEmpresaCabecera.class);
		return q.getResultList();
	}

	public List<CdcClasificacion> findAllCdcClasificacion() {
		TypedQuery<CdcClasificacion> q = em.createQuery("SELECT c FROM CdcClasificacion c", CdcClasificacion.class);
		return q.getResultList();
	}

	public List<CdcCategoria> findAllCdcCategoria() {
		TypedQuery<CdcCategoria> q = em.createQuery("SELECT c FROM CdcCategoria c", CdcCategoria.class);
		return q.getResultList();
	}

	public List<CdcEmpresaDetalle> findAllCdcEmpresaDetalle() {
		TypedQuery<CdcEmpresaDetalle> q = em.createQuery("SELECT c FROM CdcEmpresaDetalle c", CdcEmpresaDetalle.class);
		return q.getResultList();
	}

	public DTODetallesEmpresas crearDetalleEmpresas(int idUbicacion, String tipo, String telefono, String correo) {
		CdgCiudad ola = em.find(CdgCiudad.class, idUbicacion);

		DTODetallesEmpresas detalle = new DTODetallesEmpresas();
		detalle.setIdUbicacion(ola.getNombreCiudad());
		detalle.setTipo(tipo);
		detalle.setTelefono(telefono);
		detalle.setCorreo(correo);
		detalle.setIdCiudad(idUbicacion);

		return detalle;
	}

	public void empresasMultipleUbicacion(List<DTODetallesEmpresas> listaDetalleEmpresas, String nombreEmpresa,
			int idUbicacion, int categoria, int clasificacion) {

		CdcCategoria cat = em.find(CdcCategoria.class, categoria);
		CdcClasificacion cla = em.find(CdcClasificacion.class, clasificacion);

		CdcEmpresaCabecera cabecera = new CdcEmpresaCabecera();
		cabecera.setNombreEmpresa(nombreEmpresa);
		cabecera.setCdcCategoria(cat);
		cabecera.setCdcClasificacion(cla);

		List<CdcEmpresaDetalle> listaDetalle = new ArrayList<CdcEmpresaDetalle>();
		cabecera.setCdcEmpresaDetalles(listaDetalle);

		for (DTODetallesEmpresas det : listaDetalleEmpresas) {
			CdcEmpresaDetalle malDet = new CdcEmpresaDetalle();
			malDet.setCdcEmpresaCabecera(cabecera);
			CdgCiudad ciu = em.find(CdgCiudad.class, det.getIdCiudad());
			malDet.setCdgCiudad(ciu);
			malDet.setTipo(det.getTipo());
			malDet.setTelefono(det.getTelefono());
			malDet.setCorreo(det.getCorreo());
			listaDetalle.add(malDet);
		}
		em.persist(cabecera);
	}

}
