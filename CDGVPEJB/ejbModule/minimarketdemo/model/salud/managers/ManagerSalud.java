package minimarketdemo.model.salud.managers;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import minimarketdemo.model.core.entities.SalCarnet;

/**
 * Session Bean implementation class ManagerSalud
 */
@Stateless
@LocalBean
public class ManagerSalud {
	
	@PersistenceContext
	private EntityManager eManager;

    /**
     * Default constructor. 
     */
    public ManagerSalud() {
        // TODO Auto-generated constructor stub
    }
    
    //metodos para secretaria
    public void insertarCarnet(SalCarnet carnet) {
    	carnet.setEstado(false);
		eManager.persist(carnet);
	}
 
    public List<SalCarnet> findAllCarnet(){
    	return eManager.createNamedQuery("SalCarnet.findAll", SalCarnet.class).getResultList();
    }
    
    //m�todos para la enfermera
    public void validarCarnet(int idSalCarnet) {
    	SalCarnet carnet= eManager.find(SalCarnet.class, idSalCarnet);
    	carnet.setEstado(true);
    	eManager.merge(carnet);
    }
}
