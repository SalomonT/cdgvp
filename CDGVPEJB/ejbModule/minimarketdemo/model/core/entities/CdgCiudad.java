package minimarketdemo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the cdg_ciudad database table.
 * 
 */
@Entity
@Table(name="cdg_ciudad")
@NamedQuery(name="CdgCiudad.findAll", query="SELECT c FROM CdgCiudad c")
public class CdgCiudad implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_cdg_ciudad", unique=true, nullable=false)
	private Integer idCdgCiudad;

	@Column(name="nombre_ciudad", nullable=false, length=50)
	private String nombreCiudad;

	//bi-directional many-to-one association to CdcEmpresaDetalle
	@OneToMany(mappedBy="cdgCiudad")
	private List<CdcEmpresaDetalle> cdcEmpresaDetalles;

	//bi-directional many-to-one association to CdgProvincia
	@ManyToOne
	@JoinColumn(name="id_cdg_provincia")
	private CdgProvincia cdgProvincia;

	//bi-directional many-to-one association to CdgGuardiaCabecera
	@OneToMany(mappedBy="cdgCiudad")
	private List<CdgGuardiaCabecera> cdgGuardiaCabeceras;

	public CdgCiudad() {
	}

	public Integer getIdCdgCiudad() {
		return this.idCdgCiudad;
	}

	public void setIdCdgCiudad(Integer idCdgCiudad) {
		this.idCdgCiudad = idCdgCiudad;
	}

	public String getNombreCiudad() {
		return this.nombreCiudad;
	}

	public void setNombreCiudad(String nombreCiudad) {
		this.nombreCiudad = nombreCiudad;
	}

	public List<CdcEmpresaDetalle> getCdcEmpresaDetalles() {
		return this.cdcEmpresaDetalles;
	}

	public void setCdcEmpresaDetalles(List<CdcEmpresaDetalle> cdcEmpresaDetalles) {
		this.cdcEmpresaDetalles = cdcEmpresaDetalles;
	}

	public CdcEmpresaDetalle addCdcEmpresaDetalle(CdcEmpresaDetalle cdcEmpresaDetalle) {
		getCdcEmpresaDetalles().add(cdcEmpresaDetalle);
		cdcEmpresaDetalle.setCdgCiudad(this);

		return cdcEmpresaDetalle;
	}

	public CdcEmpresaDetalle removeCdcEmpresaDetalle(CdcEmpresaDetalle cdcEmpresaDetalle) {
		getCdcEmpresaDetalles().remove(cdcEmpresaDetalle);
		cdcEmpresaDetalle.setCdgCiudad(null);

		return cdcEmpresaDetalle;
	}

	public CdgProvincia getCdgProvincia() {
		return this.cdgProvincia;
	}

	public void setCdgProvincia(CdgProvincia cdgProvincia) {
		this.cdgProvincia = cdgProvincia;
	}

	public List<CdgGuardiaCabecera> getCdgGuardiaCabeceras() {
		return this.cdgGuardiaCabeceras;
	}

	public void setCdgGuardiaCabeceras(List<CdgGuardiaCabecera> cdgGuardiaCabeceras) {
		this.cdgGuardiaCabeceras = cdgGuardiaCabeceras;
	}

	public CdgGuardiaCabecera addCdgGuardiaCabecera(CdgGuardiaCabecera cdgGuardiaCabecera) {
		getCdgGuardiaCabeceras().add(cdgGuardiaCabecera);
		cdgGuardiaCabecera.setCdgCiudad(this);

		return cdgGuardiaCabecera;
	}

	public CdgGuardiaCabecera removeCdgGuardiaCabecera(CdgGuardiaCabecera cdgGuardiaCabecera) {
		getCdgGuardiaCabeceras().remove(cdgGuardiaCabecera);
		cdgGuardiaCabecera.setCdgCiudad(null);

		return cdgGuardiaCabecera;
	}

}