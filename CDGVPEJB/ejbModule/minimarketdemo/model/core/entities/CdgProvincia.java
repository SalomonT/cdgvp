package minimarketdemo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;

/**
 * The persistent class for the cdg_provincia database table.
 * 
 */
@Entity
@Table(name = "cdg_provincia")
@NamedQuery(name = "CdgProvincia.findAll", query = "SELECT c FROM CdgProvincia c")
public class CdgProvincia implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_cdg_provincia", unique = true, nullable = false)
	private Integer idCdgProvincia;

	@Column(name = "nombre_provincia", nullable = false, length = 50)
	private String nombreProvincia;

	// bi-directional many-to-one association to CdgCiudad
	@OneToMany(mappedBy = "cdgProvincia", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<CdgCiudad> cdgCiudads;

	public CdgProvincia() {
	}

	public Integer getIdCdgProvincia() {
		return this.idCdgProvincia;
	}

	public void setIdCdgProvincia(Integer idCdgProvincia) {
		this.idCdgProvincia = idCdgProvincia;
	}

	public String getNombreProvincia() {
		return this.nombreProvincia;
	}

	public void setNombreProvincia(String nombreProvincia) {
		this.nombreProvincia = nombreProvincia;
	}

	public List<CdgCiudad> getCdgCiudads() {
		return this.cdgCiudads;
	}

	public void setCdgCiudads(List<CdgCiudad> cdgCiudads) {
		this.cdgCiudads = cdgCiudads;
	}

	public CdgCiudad addCdgCiudad(CdgCiudad cdgCiudad) {
		getCdgCiudads().add(cdgCiudad);
		cdgCiudad.setCdgProvincia(this);

		return cdgCiudad;
	}

	public CdgCiudad removeCdgCiudad(CdgCiudad cdgCiudad) {
		getCdgCiudads().remove(cdgCiudad);
		cdgCiudad.setCdgProvincia(null);

		return cdgCiudad;
	}

}