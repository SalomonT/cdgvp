package minimarketdemo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the cdg_nivel_educacion database table.
 * 
 */
@Entity
@Table(name="cdg_nivel_educacion")
@NamedQuery(name="CdgNivelEducacion.findAll", query="SELECT c FROM CdgNivelEducacion c")
public class CdgNivelEducacion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_cdg_nivel_educacion", unique=true, nullable=false)
	private Integer idCdgNivelEducacion;

	@Column(nullable=false, length=50)
	private String carrera;

	@Column(nullable=false, length=25)
	private String nivel;

	@Column(nullable=false)
	private Boolean registrado;

	//bi-directional many-to-one association to CdgGuardiaCabecera
	@OneToMany(mappedBy="cdgNivelEducacion")
	private List<CdgGuardiaCabecera> cdgGuardiaCabeceras;

	public CdgNivelEducacion() {
	}

	public Integer getIdCdgNivelEducacion() {
		return this.idCdgNivelEducacion;
	}

	public void setIdCdgNivelEducacion(Integer idCdgNivelEducacion) {
		this.idCdgNivelEducacion = idCdgNivelEducacion;
	}

	public String getCarrera() {
		return this.carrera;
	}

	public void setCarrera(String carrera) {
		this.carrera = carrera;
	}

	public String getNivel() {
		return this.nivel;
	}

	public void setNivel(String nivel) {
		this.nivel = nivel;
	}

	public Boolean getRegistrado() {
		return this.registrado;
	}

	public void setRegistrado(Boolean registrado) {
		this.registrado = registrado;
	}

	public List<CdgGuardiaCabecera> getCdgGuardiaCabeceras() {
		return this.cdgGuardiaCabeceras;
	}

	public void setCdgGuardiaCabeceras(List<CdgGuardiaCabecera> cdgGuardiaCabeceras) {
		this.cdgGuardiaCabeceras = cdgGuardiaCabeceras;
	}

	public CdgGuardiaCabecera addCdgGuardiaCabecera(CdgGuardiaCabecera cdgGuardiaCabecera) {
		getCdgGuardiaCabeceras().add(cdgGuardiaCabecera);
		cdgGuardiaCabecera.setCdgNivelEducacion(this);

		return cdgGuardiaCabecera;
	}

	public CdgGuardiaCabecera removeCdgGuardiaCabecera(CdgGuardiaCabecera cdgGuardiaCabecera) {
		getCdgGuardiaCabeceras().remove(cdgGuardiaCabecera);
		cdgGuardiaCabecera.setCdgNivelEducacion(null);

		return cdgGuardiaCabecera;
	}

}