package minimarketdemo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the cdc_categoria database table.
 * 
 */
@Entity
@Table(name="cdc_categoria")
@NamedQuery(name="CdcCategoria.findAll", query="SELECT c FROM CdcCategoria c")
public class CdcCategoria implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_cdg_categoria", unique=true, nullable=false)
	private Integer idCdgCategoria;

	@Column(name="nombre_categoria", nullable=false, length=50)
	private String nombreCategoria;

	//bi-directional many-to-one association to CdcEmpresaCabecera
	@OneToMany(mappedBy="cdcCategoria",cascade = CascadeType.ALL,fetch = FetchType.EAGER)
	private List<CdcEmpresaCabecera> cdcEmpresaCabeceras;

	public CdcCategoria() {
	}

	public Integer getIdCdgCategoria() {
		return this.idCdgCategoria;
	}

	public void setIdCdgCategoria(Integer idCdgCategoria) {
		this.idCdgCategoria = idCdgCategoria;
	}

	public String getNombreCategoria() {
		return this.nombreCategoria;
	}

	public void setNombreCategoria(String nombreCategoria) {
		this.nombreCategoria = nombreCategoria;
	}

	public List<CdcEmpresaCabecera> getCdcEmpresaCabeceras() {
		return this.cdcEmpresaCabeceras;
	}

	public void setCdcEmpresaCabeceras(List<CdcEmpresaCabecera> cdcEmpresaCabeceras) {
		this.cdcEmpresaCabeceras = cdcEmpresaCabeceras;
	}

	public CdcEmpresaCabecera addCdcEmpresaCabecera(CdcEmpresaCabecera cdcEmpresaCabecera) {
		getCdcEmpresaCabeceras().add(cdcEmpresaCabecera);
		cdcEmpresaCabecera.setCdcCategoria(this);

		return cdcEmpresaCabecera;
	}

	public CdcEmpresaCabecera removeCdcEmpresaCabecera(CdcEmpresaCabecera cdcEmpresaCabecera) {
		getCdcEmpresaCabeceras().remove(cdcEmpresaCabecera);
		cdcEmpresaCabecera.setCdcCategoria(null);

		return cdcEmpresaCabecera;
	}

}