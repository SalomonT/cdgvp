package minimarketdemo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the cdg_guardia_detalle database table.
 * 
 */
@Entity
@Table(name="cdg_guardia_detalle")
@NamedQuery(name="CdgGuardiaDetalle.findAll", query="SELECT c FROM CdgGuardiaDetalle c")
public class CdgGuardiaDetalle implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_cdg_guardia_detalle", unique=true, nullable=false)
	private Integer idCdgGuardiaDetalle;

	//bi-directional many-to-one association to CdcEmpresaDetalle
	@ManyToOne
	@JoinColumn(name="id_cdg_empresa_detalle")
	private CdcEmpresaDetalle cdcEmpresaDetalle;

	//bi-directional many-to-one association to CdgGuardiaCabecera
	@ManyToOne
	@JoinColumn(name="cedula")
	private CdgGuardiaCabecera cdgGuardiaCabecera;

	public CdgGuardiaDetalle() {
	}

	public Integer getIdCdgGuardiaDetalle() {
		return this.idCdgGuardiaDetalle;
	}

	public void setIdCdgGuardiaDetalle(Integer idCdgGuardiaDetalle) {
		this.idCdgGuardiaDetalle = idCdgGuardiaDetalle;
	}

	public CdcEmpresaDetalle getCdcEmpresaDetalle() {
		return this.cdcEmpresaDetalle;
	}

	public void setCdcEmpresaDetalle(CdcEmpresaDetalle cdcEmpresaDetalle) {
		this.cdcEmpresaDetalle = cdcEmpresaDetalle;
	}

	public CdgGuardiaCabecera getCdgGuardiaCabecera() {
		return this.cdgGuardiaCabecera;
	}

	public void setCdgGuardiaCabecera(CdgGuardiaCabecera cdgGuardiaCabecera) {
		this.cdgGuardiaCabecera = cdgGuardiaCabecera;
	}

}