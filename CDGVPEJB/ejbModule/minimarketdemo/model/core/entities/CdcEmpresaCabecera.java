package minimarketdemo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the cdc_empresa_cabecera database table.
 * 
 */
@Entity
@Table(name="cdc_empresa_cabecera")
@NamedQuery(name="CdcEmpresaCabecera.findAll", query="SELECT c FROM CdcEmpresaCabecera c")
public class CdcEmpresaCabecera implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="nombre_empresa", unique=true, nullable=false, length=50)
	private String nombreEmpresa;

	//bi-directional many-to-one association to CdcCategoria
	@ManyToOne
	@JoinColumn(name="id_cdg_categoria")
	private CdcCategoria cdcCategoria;

	//bi-directional many-to-one association to CdcClasificacion
	@ManyToOne
	@JoinColumn(name="id_cdg_clasificacion")
	private CdcClasificacion cdcClasificacion;

	//bi-directional many-to-one association to CdcEmpresaDetalle
	@OneToMany(mappedBy="cdcEmpresaCabecera",cascade = CascadeType.ALL,fetch = FetchType.EAGER)
	private List<CdcEmpresaDetalle> cdcEmpresaDetalles;

	public CdcEmpresaCabecera() {
	}

	public String getNombreEmpresa() {
		return this.nombreEmpresa;
	}

	public void setNombreEmpresa(String nombreEmpresa) {
		this.nombreEmpresa = nombreEmpresa;
	}

	public CdcCategoria getCdcCategoria() {
		return this.cdcCategoria;
	}

	public void setCdcCategoria(CdcCategoria cdcCategoria) {
		this.cdcCategoria = cdcCategoria;
	}

	public CdcClasificacion getCdcClasificacion() {
		return this.cdcClasificacion;
	}

	public void setCdcClasificacion(CdcClasificacion cdcClasificacion) {
		this.cdcClasificacion = cdcClasificacion;
	}

	public List<CdcEmpresaDetalle> getCdcEmpresaDetalles() {
		return this.cdcEmpresaDetalles;
	}

	public void setCdcEmpresaDetalles(List<CdcEmpresaDetalle> cdcEmpresaDetalles) {
		this.cdcEmpresaDetalles = cdcEmpresaDetalles;
	}

	public CdcEmpresaDetalle addCdcEmpresaDetalle(CdcEmpresaDetalle cdcEmpresaDetalle) {
		getCdcEmpresaDetalles().add(cdcEmpresaDetalle);
		cdcEmpresaDetalle.setCdcEmpresaCabecera(this);

		return cdcEmpresaDetalle;
	}

	public CdcEmpresaDetalle removeCdcEmpresaDetalle(CdcEmpresaDetalle cdcEmpresaDetalle) {
		getCdcEmpresaDetalles().remove(cdcEmpresaDetalle);
		cdcEmpresaDetalle.setCdcEmpresaCabecera(null);

		return cdcEmpresaDetalle;
	}

}