package minimarketdemo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the cdc_clasificacion database table.
 * 
 */
@Entity
@Table(name="cdc_clasificacion")
@NamedQuery(name="CdcClasificacion.findAll", query="SELECT c FROM CdcClasificacion c")
public class CdcClasificacion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_cdg_clasificacion", unique=true, nullable=false)
	private Integer idCdgClasificacion;

	@Column(name="nombre_clasificacion", nullable=false, length=50)
	private String nombreClasificacion;

	//bi-directional many-to-one association to CdcEmpresaCabecera
	@OneToMany(mappedBy="cdcClasificacion",cascade = CascadeType.ALL,fetch = FetchType.EAGER)
	private List<CdcEmpresaCabecera> cdcEmpresaCabeceras;

	public CdcClasificacion() {
	}

	public Integer getIdCdgClasificacion() {
		return this.idCdgClasificacion;
	}

	public void setIdCdgClasificacion(Integer idCdgClasificacion) {
		this.idCdgClasificacion = idCdgClasificacion;
	}

	public String getNombreClasificacion() {
		return this.nombreClasificacion;
	}

	public void setNombreClasificacion(String nombreClasificacion) {
		this.nombreClasificacion = nombreClasificacion;
	}

	public List<CdcEmpresaCabecera> getCdcEmpresaCabeceras() {
		return this.cdcEmpresaCabeceras;
	}

	public void setCdcEmpresaCabeceras(List<CdcEmpresaCabecera> cdcEmpresaCabeceras) {
		this.cdcEmpresaCabeceras = cdcEmpresaCabeceras;
	}

	public CdcEmpresaCabecera addCdcEmpresaCabecera(CdcEmpresaCabecera cdcEmpresaCabecera) {
		getCdcEmpresaCabeceras().add(cdcEmpresaCabecera);
		cdcEmpresaCabecera.setCdcClasificacion(this);

		return cdcEmpresaCabecera;
	}

	public CdcEmpresaCabecera removeCdcEmpresaCabecera(CdcEmpresaCabecera cdcEmpresaCabecera) {
		getCdcEmpresaCabeceras().remove(cdcEmpresaCabecera);
		cdcEmpresaCabecera.setCdcClasificacion(null);

		return cdcEmpresaCabecera;
	}

}