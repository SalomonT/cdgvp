package minimarketdemo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the cdc_empresa_detalle database table.
 * 
 */
@Entity
@Table(name="cdc_empresa_detalle")
@NamedQuery(name="CdcEmpresaDetalle.findAll", query="SELECT c FROM CdcEmpresaDetalle c")
public class CdcEmpresaDetalle implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_cdg_empresa_detalle", unique=true, nullable=false)
	private Integer idCdgEmpresaDetalle;

	@Column(nullable=false, length=50)
	private String correo;

	@Column(nullable=false, length=7)
	private String telefono;

	@Column(nullable=false, length=20)
	private String tipo;

	//bi-directional many-to-one association to CdcEmpresaCabecera
	@ManyToOne
	@JoinColumn(name="nombre_empresa")
	private CdcEmpresaCabecera cdcEmpresaCabecera;

	//bi-directional many-to-one association to CdgCiudad
	@ManyToOne
	@JoinColumn(name="id_cdg_ciudad")
	private CdgCiudad cdgCiudad;

	//bi-directional many-to-one association to CdgGuardiaDetalle
	@OneToMany(mappedBy="cdcEmpresaDetalle")
	private List<CdgGuardiaDetalle> cdgGuardiaDetalles;

	public CdcEmpresaDetalle() {
	}

	public Integer getIdCdgEmpresaDetalle() {
		return this.idCdgEmpresaDetalle;
	}

	public void setIdCdgEmpresaDetalle(Integer idCdgEmpresaDetalle) {
		this.idCdgEmpresaDetalle = idCdgEmpresaDetalle;
	}

	public String getCorreo() {
		return this.correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getTelefono() {
		return this.telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getTipo() {
		return this.tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public CdcEmpresaCabecera getCdcEmpresaCabecera() {
		return this.cdcEmpresaCabecera;
	}

	public void setCdcEmpresaCabecera(CdcEmpresaCabecera cdcEmpresaCabecera) {
		this.cdcEmpresaCabecera = cdcEmpresaCabecera;
	}

	public CdgCiudad getCdgCiudad() {
		return this.cdgCiudad;
	}

	public void setCdgCiudad(CdgCiudad cdgCiudad) {
		this.cdgCiudad = cdgCiudad;
	}

	public List<CdgGuardiaDetalle> getCdgGuardiaDetalles() {
		return this.cdgGuardiaDetalles;
	}

	public void setCdgGuardiaDetalles(List<CdgGuardiaDetalle> cdgGuardiaDetalles) {
		this.cdgGuardiaDetalles = cdgGuardiaDetalles;
	}

	public CdgGuardiaDetalle addCdgGuardiaDetalle(CdgGuardiaDetalle cdgGuardiaDetalle) {
		getCdgGuardiaDetalles().add(cdgGuardiaDetalle);
		cdgGuardiaDetalle.setCdcEmpresaDetalle(this);

		return cdgGuardiaDetalle;
	}

	public CdgGuardiaDetalle removeCdgGuardiaDetalle(CdgGuardiaDetalle cdgGuardiaDetalle) {
		getCdgGuardiaDetalles().remove(cdgGuardiaDetalle);
		cdgGuardiaDetalle.setCdcEmpresaDetalle(null);

		return cdgGuardiaDetalle;
	}

}