package minimarketdemo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the cdg_historial database table.
 * 
 */
@Entity
@Table(name="cdg_historial")
@NamedQuery(name="CdgHistorial.findAll", query="SELECT c FROM CdgHistorial c")
public class CdgHistorial implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_cdg_historial", unique=true, nullable=false)
	private Integer idCdgHistorial;

	@Column(nullable=false)
	private Boolean estado;

	@Temporal(TemporalType.DATE)
	@Column(name="fecha_ingreso", nullable=false)
	private Date fechaIngreso;

	@Temporal(TemporalType.DATE)
	@Column(name="fecha_salida", nullable=false)
	private Date fechaSalida;

	//bi-directional many-to-one association to CdgGuardiaCabecera
	@ManyToOne
	@JoinColumn(name="cedula")
	private CdgGuardiaCabecera cdgGuardiaCabecera;

	public CdgHistorial() {
	}

	public Integer getIdCdgHistorial() {
		return this.idCdgHistorial;
	}

	public void setIdCdgHistorial(Integer idCdgHistorial) {
		this.idCdgHistorial = idCdgHistorial;
	}

	public Boolean getEstado() {
		return this.estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	public Date getFechaIngreso() {
		return this.fechaIngreso;
	}

	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public Date getFechaSalida() {
		return this.fechaSalida;
	}

	public void setFechaSalida(Date fechaSalida) {
		this.fechaSalida = fechaSalida;
	}

	public CdgGuardiaCabecera getCdgGuardiaCabecera() {
		return this.cdgGuardiaCabecera;
	}

	public void setCdgGuardiaCabecera(CdgGuardiaCabecera cdgGuardiaCabecera) {
		this.cdgGuardiaCabecera = cdgGuardiaCabecera;
	}

}