package minimarketdemo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the cdg_guardia_cabecera database table.
 * 
 */
@Entity
@Table(name="cdg_guardia_cabecera")
@NamedQuery(name="CdgGuardiaCabecera.findAll", query="SELECT c FROM CdgGuardiaCabecera c")
public class CdgGuardiaCabecera implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(unique=true, nullable=false, length=10)
	private String cedula;

	@Column(nullable=false, length=50)
	private String apellidos;

	@Column(nullable=false, length=10)
	private String celular;

	@Column(nullable=false)
	private Boolean discapacidad;

	@Temporal(TemporalType.DATE)
	@Column(name="fecha_nacimiento", nullable=false)
	private Date fechaNacimiento;

	@Column(nullable=false, length=50)
	private String genero;

	@Column(nullable=false, length=50)
	private String nombres;

	@Column(name="porcentaje_discapacidad", nullable=false)
	private Integer porcentajeDiscapacidad;

	@Column(nullable=false)
	private Boolean reentrenado;

	//bi-directional many-to-one association to CdgCiudad
	@ManyToOne
	@JoinColumn(name="id_cdg_ciudad")
	private CdgCiudad cdgCiudad;

	//bi-directional many-to-one association to CdgNivelEducacion
	@ManyToOne
	@JoinColumn(name="id_cdg_nivel_educacion")
	private CdgNivelEducacion cdgNivelEducacion;

	//bi-directional many-to-one association to CdgGuardiaDetalle
	@OneToMany(mappedBy="cdgGuardiaCabecera",cascade = CascadeType.ALL,fetch = FetchType.EAGER)
	private List<CdgGuardiaDetalle> cdgGuardiaDetalles;

	//bi-directional many-to-one association to CdgHistorial
	@OneToMany(mappedBy="cdgGuardiaCabecera")
	private List<CdgHistorial> cdgHistorials;

	public CdgGuardiaCabecera() {
	}

	public String getCedula() {
		return this.cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	public String getApellidos() {
		return this.apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getCelular() {
		return this.celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public Boolean getDiscapacidad() {
		return this.discapacidad;
	}

	public void setDiscapacidad(Boolean discapacidad) {
		this.discapacidad = discapacidad;
	}

	public Date getFechaNacimiento() {
		return this.fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getGenero() {
		return this.genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public String getNombres() {
		return this.nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public Integer getPorcentajeDiscapacidad() {
		return this.porcentajeDiscapacidad;
	}

	public void setPorcentajeDiscapacidad(Integer porcentajeDiscapacidad) {
		this.porcentajeDiscapacidad = porcentajeDiscapacidad;
	}

	public Boolean getReentrenado() {
		return this.reentrenado;
	}

	public void setReentrenado(Boolean reentrenado) {
		this.reentrenado = reentrenado;
	}

	public CdgCiudad getCdgCiudad() {
		return this.cdgCiudad;
	}

	public void setCdgCiudad(CdgCiudad cdgCiudad) {
		this.cdgCiudad = cdgCiudad;
	}

	public CdgNivelEducacion getCdgNivelEducacion() {
		return this.cdgNivelEducacion;
	}

	public void setCdgNivelEducacion(CdgNivelEducacion cdgNivelEducacion) {
		this.cdgNivelEducacion = cdgNivelEducacion;
	}

	public List<CdgGuardiaDetalle> getCdgGuardiaDetalles() {
		return this.cdgGuardiaDetalles;
	}

	public void setCdgGuardiaDetalles(List<CdgGuardiaDetalle> cdgGuardiaDetalles) {
		this.cdgGuardiaDetalles = cdgGuardiaDetalles;
	}

	public CdgGuardiaDetalle addCdgGuardiaDetalle(CdgGuardiaDetalle cdgGuardiaDetalle) {
		getCdgGuardiaDetalles().add(cdgGuardiaDetalle);
		cdgGuardiaDetalle.setCdgGuardiaCabecera(this);

		return cdgGuardiaDetalle;
	}

	public CdgGuardiaDetalle removeCdgGuardiaDetalle(CdgGuardiaDetalle cdgGuardiaDetalle) {
		getCdgGuardiaDetalles().remove(cdgGuardiaDetalle);
		cdgGuardiaDetalle.setCdgGuardiaCabecera(null);

		return cdgGuardiaDetalle;
	}

	public List<CdgHistorial> getCdgHistorials() {
		return this.cdgHistorials;
	}

	public void setCdgHistorials(List<CdgHistorial> cdgHistorials) {
		this.cdgHistorials = cdgHistorials;
	}

	public CdgHistorial addCdgHistorial(CdgHistorial cdgHistorial) {
		getCdgHistorials().add(cdgHistorial);
		cdgHistorial.setCdgGuardiaCabecera(this);

		return cdgHistorial;
	}

	public CdgHistorial removeCdgHistorial(CdgHistorial cdgHistorial) {
		getCdgHistorials().remove(cdgHistorial);
		cdgHistorial.setCdgGuardiaCabecera(null);

		return cdgHistorial;
	}

}