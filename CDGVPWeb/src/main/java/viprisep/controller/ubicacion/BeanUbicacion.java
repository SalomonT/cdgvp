package viprisep.controller.ubicacion;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.model.SelectItem;
import javax.faces.model.SelectItemGroup;
import javax.inject.Named;

import minimarketdemo.model.core.entities.CdgCiudad;
import minimarketdemo.model.core.entities.CdgGuardiaCabecera;
import minimarketdemo.model.core.entities.CdgProvincia;
import viprisep.controller.util.JSFUtil;
import viprisep.model.guardias.dtos.DTODetalleUbicacion;
import viprisep.model.ubicacion.managers.ManagerUbicacion;

@Named
@SessionScoped
public class BeanUbicacion implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private ManagerUbicacion managerUbicacion;

	private List<SelectItem> provincias;
	private String selection;

	private List<DTODetalleUbicacion> listaDetalleUbicacion;
	private int idCiudad;
	private String nombreProvincia;
	private String nombreCiudad;
	private int idProvinciaSeleccionada;
	private List<CdgProvincia> listaProvincia;
	private List<SelectItem> listaProvincias;
	private List<CdgProvincia> provinciasAgregadas;
	private CdgCiudad ciudadEdicion;
	private int ciudadSeleccionada;

	private List<CdgProvincia> listaProvinciacab;
	private List<CdgCiudad> listaCiudades;

	public BeanUbicacion() {
		// TODO Auto-generated constructor stub
	}

	@PostConstruct
	public void inicializar() {
		listaDetalleUbicacion = new ArrayList<DTODetalleUbicacion>();
		listaProvincia = managerUbicacion.findAllCdgProvincia();
		listaCiudades = managerUbicacion.findAllCdgCiudad();

		provincias = new ArrayList<>();
		SelectItemGroup group1 = new SelectItemGroup("Azuay");
		// group1.setValue(new Place("Azuay", "azu", "provincia"));

	}

	public void actionListenerSeleccionarCiudad(CdgCiudad ciudad) {
		ciudadEdicion = ciudad;
		ciudadSeleccionada = ciudad.getIdCdgCiudad();
		System.out.println(ciudadSeleccionada);
	}

	public void actionListenerGuardarUbicacion() {
		managerUbicacion.ubicacionMultipleGuardia(listaDetalleUbicacion, idProvinciaSeleccionada, nombreCiudad);
		listaProvinciacab = managerUbicacion.findAllCdgProvincia();
		listaDetalleUbicacion = new ArrayList<DTODetalleUbicacion>();
		nombreCiudad = "";
		listaProvincia = managerUbicacion.findAllCdgProvincia();
		listaCiudades = managerUbicacion.findAllCdgCiudad();
		JSFUtil.crearMensajeInfo("Ciudad Guardada correctamente.");
	}

	public void actionListenerActualizarCiudad() {
		managerUbicacion.actualizarCiudad(ciudadEdicion);
		listaCiudades = managerUbicacion.findAllCdgCiudad();
		JSFUtil.crearMensajeInfo("Ciudad actualizada correctamente.");
	}

	public void actionListenerEliminarCiudad(int idCiudad) {
		System.out.println("buscando...");
		managerUbicacion.EliminarCiudad(idCiudad);
		listaCiudades = managerUbicacion.findAllCdgCiudad();
		System.out.println("Ciudad eliminadaAAA");
		System.out.println(idCiudad);
	}

	public void actionListenerAgregarUbicacioncab() {
		listaDetalleUbicacion.add(managerUbicacion.crearDetalleUbicacion(idProvinciaSeleccionada, nombreCiudad));
		nombreCiudad = "";
		JSFUtil.crearMensajeInfo("Ciudad Agregada.");
	}

	public List<DTODetalleUbicacion> getListaDetalleUbicacion() {
		return listaDetalleUbicacion;
	}

	public void setListaDetalleUbicacion(List<DTODetalleUbicacion> listaDetalleUbicacion) {
		this.listaDetalleUbicacion = listaDetalleUbicacion;
	}

	public int getIdCiudad() {
		return idCiudad;
	}

	public void setIdCiudad(int idCiudad) {
		this.idCiudad = idCiudad;
	}

	public String getNombreCiudad() {
		return nombreCiudad;
	}

	public void setNombreCiudad(String nombreCiudad) {
		this.nombreCiudad = nombreCiudad;
	}

	public String getNombreProvincia() {
		return nombreProvincia;
	}

	public void setNombreProvincia(String nombreProvincia) {
		this.nombreProvincia = nombreProvincia;
	}

	public int getIdProvinciaSeleccionada() {
		return idProvinciaSeleccionada;
	}

	public void setIdProvinciaSeleccionada(int idProvinciaSeleccionada) {
		this.idProvinciaSeleccionada = idProvinciaSeleccionada;
	}

	public List<CdgProvincia> getListaProvincia() {
		return listaProvincia;
	}

	public void setListaProvincia(List<CdgProvincia> listaProvincia) {
		this.listaProvincia = listaProvincia;
	}

	public List<CdgCiudad> getListaCiudades() {
		return listaCiudades;
	}

	public void setListaCiudades(List<CdgCiudad> listaCiudades) {
		this.listaCiudades = listaCiudades;
	}

	public CdgCiudad getCiudadEdicion() {
		return ciudadEdicion;
	}

	public void setCiudadEdicion(CdgCiudad ciudadEdicion) {
		this.ciudadEdicion = ciudadEdicion;
	}

	public List<SelectItem> getListaProvincias() {
		return listaProvincias;
	}

	public void setListaProvincias(List<SelectItem> listaProvincias) {
		this.listaProvincias = listaProvincias;
	}

	public List<CdgProvincia> getProvinciasAgregadas() {
		return provinciasAgregadas;
	}

	public void setProvinciasAgregadas(List<CdgProvincia> provinciasAgregadas) {
		this.provinciasAgregadas = provinciasAgregadas;
	}

	public int getCiudadSeleccionada() {
		return ciudadSeleccionada;
	}

	public void setCiudadSeleccionada(int ciudadSeleccionada) {
		this.ciudadSeleccionada = ciudadSeleccionada;
	}

	public List<CdgProvincia> getListaProvinciacab() {
		return listaProvinciacab;
	}

	public void setListaProvinciacab(List<CdgProvincia> listaProvinciacab) {
		this.listaProvinciacab = listaProvinciacab;
	}

}
