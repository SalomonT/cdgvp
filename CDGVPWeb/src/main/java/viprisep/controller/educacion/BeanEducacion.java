/**
 * 
 */
package viprisep.controller.educacion;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import minimarketdemo.model.core.entities.CdgNivelEducacion;
import viprisep.controller.util.JSFUtil;
import viprisep.model.educacion.managers.ManagerEducacion;

@Named
@SessionScoped
/**
 * @author rglimac
 *
 */
public class BeanEducacion implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private ManagerEducacion mEducacion;

	private List<CdgNivelEducacion> listaEducacion;

	private String nivel;
	private String carrera;
	private Boolean registrado;
	private CdgNivelEducacion eduSeleccionado;
	private int idEducacionEditar;

	@PostConstruct
	public void inicializar() {
		listaEducacion = mEducacion.findAllCdgNivelEducacion();

	}

	public BeanEducacion() {
		// TODO Auto-generated constructor stub
	}

	public CdgNivelEducacion getEduSeleccionado() {
		return eduSeleccionado;
	}

	public void setEduSeleccionado(CdgNivelEducacion eduSeleccionado) {
		this.eduSeleccionado = eduSeleccionado;

	}

	public void actionEliminarEdu(Integer IdEdu) {
		System.out.println("buscando...");
		mEducacion.EliminarEdu(IdEdu);
		listaEducacion = mEducacion.findAllCdgNivelEducacion();
		System.out.println("Educacion eliminada");
		JSFUtil.crearMensajeInfo("Educacion eliminada correctamente.");
	}

	public void actionGuardarEducacion() {
		System.out.println("Empezando a guardar");
		mEducacion.guardarEducacion(nivel, carrera, registrado);
		System.out.print("Herramienta guardada");
		listaEducacion = mEducacion.findAllCdgNivelEducacion();
		JSFUtil.crearMensajeInfo("Educacion Agregada Correctamente.");
	}

	public void actionListenerSeleccionarEducacion(CdgNivelEducacion educacion) {
		eduSeleccionado = educacion;
		idEducacionEditar = educacion.getIdCdgNivelEducacion();
		System.out.println(idEducacionEditar);
	}

	public void actionListenerActualizarEdu() {
		mEducacion.actualizarEdu(eduSeleccionado);
		listaEducacion = mEducacion.findAllCdgNivelEducacion();
		JSFUtil.crearMensajeInfo("Educacion actualiazada Correctamente.");
	}

	public List<CdgNivelEducacion> getListaEducacion() {
		return listaEducacion;
	}

	public void setListaEducacion(List<CdgNivelEducacion> listaEducacion) {
		this.listaEducacion = listaEducacion;
	}

	public String getNivel() {
		return nivel;
	}

	public void setNivel(String nivel) {
		this.nivel = nivel;
	}

	public String getCarrera() {
		return carrera;
	}

	public void setCarrera(String carrera) {
		this.carrera = carrera;
	}

	public Boolean getRegistrado() {
		return registrado;
	}

	public void setRegistrado(Boolean registrado) {
		this.registrado = registrado;
	}

	public int getIdEducacionEditar() {
		return idEducacionEditar;
	}

	public void setIdEducacionEditar(int idEducacionEditar) {
		this.idEducacionEditar = idEducacionEditar;
	}

}
