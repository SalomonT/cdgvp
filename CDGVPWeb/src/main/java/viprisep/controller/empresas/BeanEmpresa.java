package viprisep.controller.empresas;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import minimarketdemo.model.core.entities.CdcCategoria;
import minimarketdemo.model.core.entities.CdcClasificacion;
import minimarketdemo.model.core.entities.CdcEmpresaCabecera;
import minimarketdemo.model.core.entities.CdcEmpresaDetalle;
import minimarketdemo.model.core.entities.CdgProvincia;
import viprisep.controller.util.JSFUtil;
import viprisep.model.empresas.managers.ManagerEmpresas;
import viprisep.model.guardias.dtos.DTODetalleUbicacion;
import viprisep.model.guardias.dtos.DTODetallesEmpresas;

@Named
@SessionScoped
public class BeanEmpresa implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private ManagerEmpresas mEmpresas;
	private int idUbicacion;
	private String tipo;
	private String telefono;
	private String correo;
	private String nombreEmpresa;
	private int categoria;
	private int clasificacion;

	private List<CdcClasificacion> listaClasificacion;
	private List<CdcCategoria> listaCategoria;

	private List<DTODetallesEmpresas> listaDetalleEmpresa;

	private List<CdcEmpresaDetalle> listaEmpresaDetalles;

	private List<CdcEmpresaCabecera> listaEmpresacab;

	public BeanEmpresa() {
		// TODO Auto-generated constructor stub
	}

	@PostConstruct
	public void inicializar() {
		listaDetalleEmpresa = new ArrayList<DTODetallesEmpresas>();
		listaEmpresaDetalles = mEmpresas.findAllCdcEmpresaDetalle();
		listaCategoria = mEmpresas.findAllCdcCategoria();
		listaClasificacion = mEmpresas.findAllCdcClasificacion();
	}

	public void actionListenerGuardarEmpresa() {
		mEmpresas.empresasMultipleUbicacion(listaDetalleEmpresa, nombreEmpresa, idUbicacion, categoria, clasificacion);
		listaEmpresacab = mEmpresas.findAllCdcEmpresaCabecera();
		listaDetalleEmpresa = new ArrayList<DTODetallesEmpresas>();
		listaEmpresaDetalles = mEmpresas.findAllCdcEmpresaDetalle();
		JSFUtil.crearMensajeInfo("Sucursal Agregada Correctamente.");
	}

	public void actionListenerAgregarEmpresacab() {
		listaDetalleEmpresa.add(mEmpresas.crearDetalleEmpresas(idUbicacion, tipo, telefono, correo));
		JSFUtil.crearMensajeInfo("Empresa Agregada Correctamente.");
	}

	public List<CdcClasificacion> getListaClasificacion() {
		return listaClasificacion;
	}

	public void setListaClasificacion(List<CdcClasificacion> listaClasificacion) {
		this.listaClasificacion = listaClasificacion;
	}

	public List<CdcCategoria> getListaCategoria() {
		return listaCategoria;
	}

	public void setListaCategoria(List<CdcCategoria> listaCategoria) {
		this.listaCategoria = listaCategoria;
	}

	public int getIdUbicacion() {
		return idUbicacion;
	}

	public void setIdUbicacion(int idUbicacion) {
		this.idUbicacion = idUbicacion;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getNombreEmpresa() {
		return nombreEmpresa;
	}

	public void setNombreEmpresa(String nombreEmpresa) {
		this.nombreEmpresa = nombreEmpresa;
	}

	public List<DTODetallesEmpresas> getListaDetalleEmpresa() {
		return listaDetalleEmpresa;
	}

	public void setListaDetalleEmpresa(List<DTODetallesEmpresas> listaDetalleEmpresa) {
		this.listaDetalleEmpresa = listaDetalleEmpresa;
	}

	public int getCategoria() {
		return categoria;
	}

	public void setCategoria(int categoria) {
		this.categoria = categoria;
	}

	public int getClasificacion() {
		return clasificacion;
	}

	public void setClasificacion(int clasificacion) {
		this.clasificacion = clasificacion;
	}

	public List<CdcEmpresaCabecera> getListaEmpresacab() {
		return listaEmpresacab;
	}

	public void setListaEmpresacab(List<CdcEmpresaCabecera> listaEmpresacab) {
		this.listaEmpresacab = listaEmpresacab;
	}

	public List<CdcEmpresaDetalle> getListaEmpresaDetalles() {
		return listaEmpresaDetalles;
	}

	public void setListaEmpresaDetalles(List<CdcEmpresaDetalle> listaEmpresaDetalles) {
		this.listaEmpresaDetalles = listaEmpresaDetalles;
	}

}
