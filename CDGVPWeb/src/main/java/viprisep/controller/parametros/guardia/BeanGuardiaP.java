package viprisep.controller.parametros.guardia;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import minimarketdemo.model.core.entities.CdcClasificacion;
import minimarketdemo.model.core.entities.CdcEmpresaDetalle;
import minimarketdemo.model.core.entities.CdgCiudad;
import minimarketdemo.model.core.entities.CdgGuardiaCabecera;
import minimarketdemo.model.core.entities.CdgGuardiaDetalle;
import minimarketdemo.model.core.entities.CdgNivelEducacion;
import viprisep.controller.util.JSFUtil;
import viprisep.model.educacion.managers.ManagerEducacion;
import viprisep.model.guardias.dtos.DTODetallesGuardia;
import viprisep.model.parametros.guardias.managers.ManagerGuardiaP;

@Named
@SessionScoped
public class BeanGuardiaP implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	ManagerGuardiaP mGuardiaP;

	private List<CdgNivelEducacion> listaTitulos;
	private List<CdgCiudad> listaCiudad;
	private CdgGuardiaCabecera guardiaSeleccionado;
	private String cedulaSeleccionado;
	private List<CdgGuardiaCabecera> listaGuardiacab;

	private String cedula;
	private String Apellidos;
	private String Nombres;
	private Date fechaNacimiento;
	private String Genero;
	private String celular;
	private Boolean reentrenado;
	private Boolean discapacidad;
	private int porcentajeDiscapacidad;

	private int idTituloSeleccionado;
	private int idUbicacionSeleccionada;

	public BeanGuardiaP() {
		// TODO Auto-generated constructor stub
	}

	@PostConstruct
	public void Inicializar() {
		listaTitulos = mGuardiaP.findAllCdgNivelEducacion();
		listaGuardiacab = mGuardiaP.findAllCdgGuardiaCabecera();
		listaCiudad = mGuardiaP.findAllCdgCiudad();
	}

	public void actionGuardarGuardia() {
		System.out.println("Empezando a guardar");
		mGuardiaP.guardarGuardia(cedula, Apellidos, Nombres, fechaNacimiento, Genero, celular, reentrenado,
				discapacidad, porcentajeDiscapacidad, idTituloSeleccionado, idUbicacionSeleccionada);
		System.out.print("Guardia guardado");
		listaGuardiacab = mGuardiaP.findAllCdgGuardiaCabecera();
		JSFUtil.crearMensajeInfo("Guardia Agregado Correctamente.");
	}

	public void actionEliminarGuardia(String IdGuardia) {
		System.out.println("buscando...");
		mGuardiaP.EliminarGuardia(IdGuardia);
		listaGuardiacab = mGuardiaP.findAllCdgGuardiaCabecera();
		System.out.println("Guardia eliminado");
		JSFUtil.crearMensajeInfo("Guardia Eliminado Correctamente.");
	}

	public void actionListenerActualizarGuardia() {
		mGuardiaP.actualizarGuardiaCab(guardiaSeleccionado, idTituloSeleccionado, idUbicacionSeleccionada);
		listaGuardiacab = mGuardiaP.findAllCdgGuardiaCabecera();
		JSFUtil.crearMensajeInfo("Guardia Actualizado Correctamente.");
	}

	public void actionListenerSeleccionarGuardia(CdgGuardiaCabecera guardia) {
		guardiaSeleccionado = guardia;
		cedulaSeleccionado = guardia.getCedula();
		System.out.println(cedulaSeleccionado);
	}

	public CdgGuardiaCabecera getGuardiaSeleccionado() {
		return guardiaSeleccionado;
	}

	public void setGuardiaSeleccionado(CdgGuardiaCabecera guardiaSeleccionado) {
		this.guardiaSeleccionado = guardiaSeleccionado;
	}

	public String getCedulaSeleccionado() {
		return cedulaSeleccionado;
	}

	public void setCedulaSeleccionado(String cedulaSeleccionado) {
		this.cedulaSeleccionado = cedulaSeleccionado;
	}

	public List<CdgNivelEducacion> getListaTitulos() {
		return listaTitulos;
	}

	public void setListaTitulos(List<CdgNivelEducacion> listaTitulos) {
		this.listaTitulos = listaTitulos;
	}

	public List<CdgCiudad> getListaCiudad() {
		return listaCiudad;
	}

	public void setListaCiudad(List<CdgCiudad> listaCiudad) {
		this.listaCiudad = listaCiudad;
	}

	public List<CdgGuardiaCabecera> getListaGuardiacab() {
		return listaGuardiacab;
	}

	public void setListaGuardiacab(List<CdgGuardiaCabecera> listaGuardiacab) {
		this.listaGuardiacab = listaGuardiacab;
	}

	public String getCedula() {
		return cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	public String getApellidos() {
		return Apellidos;
	}

	public void setApellidos(String apellidos) {
		Apellidos = apellidos;
	}

	public String getNombres() {
		return Nombres;
	}

	public void setNombres(String nombres) {
		Nombres = nombres;
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getGenero() {
		return Genero;
	}

	public void setGenero(String genero) {
		Genero = genero;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public Boolean getReentrenado() {
		return reentrenado;
	}

	public void setReentrenado(Boolean reentrenado) {
		this.reentrenado = reentrenado;
	}

	public Boolean getDiscapacidad() {
		return discapacidad;
	}

	public void setDiscapacidad(Boolean discapacidad) {
		this.discapacidad = discapacidad;
	}

	public int getPorcentajeDiscapacidad() {
		return porcentajeDiscapacidad;
	}

	public void setPorcentajeDiscapacidad(int porcentajeDiscapacidad) {
		this.porcentajeDiscapacidad = porcentajeDiscapacidad;
	}

	public int getIdTituloSeleccionado() {
		return idTituloSeleccionado;
	}

	public void setIdTituloSeleccionado(int idTituloSeleccionado) {
		this.idTituloSeleccionado = idTituloSeleccionado;
	}

	public int getIdUbicacionSeleccionada() {
		return idUbicacionSeleccionada;
	}

	public void setIdUbicacionSeleccionada(int idUbicacionSeleccionada) {
		this.idUbicacionSeleccionada = idUbicacionSeleccionada;
	}

}
