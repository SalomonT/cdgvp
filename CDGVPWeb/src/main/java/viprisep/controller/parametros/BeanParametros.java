package viprisep.controller.parametros;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import minimarketdemo.model.core.entities.CdcCategoria;
import minimarketdemo.model.core.entities.CdcClasificacion;
import minimarketdemo.model.core.entities.CdgNivelEducacion;
import viprisep.controller.util.JSFUtil;
import viprisep.model.parametros.managers.ManagerParametros;

@Named
@SessionScoped
public class BeanParametros implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	ManagerParametros mParametros;

	private int categoria;
	private int clasificacion;
	private String nombreClasificacion;
	private String nombreCategoria;

	private CdcClasificacion clasiSeleccionado;
	private CdcCategoria catoSeleccionado;

	private int idClasiSeleccionado;
	private int idCatoSeleccionado;

	private List<CdcClasificacion> listaClasificacion;
	private List<CdcCategoria> listaCategoria;

	public BeanParametros() {
		// TODO Auto-generated constructor stub

	}

	@PostConstruct
	public void inicializar() {
		listaCategoria = mParametros.findAllCdcCategoria();
		listaClasificacion = mParametros.findAllCdcClasificacion();
	}

	public void actionGuardarClasificacion() {
		System.out.println("Empezando a guardar");
		mParametros.guardarClasificacion(nombreClasificacion);
		System.out.print("Herramienta guardada");
		listaClasificacion = mParametros.findAllCdcClasificacion();
		JSFUtil.crearMensajeInfo("Clasificacion Agregada Correctamente.");
	}

	public void actionGuardarCategoria() {
		System.out.println("Empezando a guardar");
		mParametros.guardarCategoria(nombreCategoria);
		System.out.print("Herramienta guardada");
		listaCategoria = mParametros.findAllCdcCategoria();
		JSFUtil.crearMensajeInfo("Categoria Agregada Correctamente.");
	}

	public void actionEliminarClasificacion(Integer IdClasificacion) {
		System.out.println("buscando...");
		mParametros.EliminarClasificacion(IdClasificacion);
		listaClasificacion = mParametros.findAllCdcClasificacion();
		System.out.println("Educacion eliminada");
		JSFUtil.crearMensajeInfo("Clasificacion Eliminada Correctamente.");
	}

	public void actionEliminarCategoria(Integer IdCategoria) {
		System.out.println("buscando...");
		mParametros.EliminarCategoria(IdCategoria);
		listaCategoria = mParametros.findAllCdcCategoria();
		System.out.println("Educacion eliminada");
		JSFUtil.crearMensajeInfo("Categoria Eliminada Correctamente.");
	}

	public void actionListenerSeleccionarClasificacion(CdcClasificacion clasificacion) {
		clasiSeleccionado = clasificacion;
		idClasiSeleccionado = clasificacion.getIdCdgClasificacion();
		System.out.println(idClasiSeleccionado);
	}

	public void actionListenerSeleccionarCategoria(CdcCategoria categoria) {
		catoSeleccionado = categoria;
		idCatoSeleccionado = categoria.getIdCdgCategoria();
		System.out.println(idCatoSeleccionado);
	}

	public void actionListenerActualizarClasificacion() {
		mParametros.actualizarClasificacion(clasiSeleccionado);
		listaClasificacion = mParametros.findAllCdcClasificacion();
		JSFUtil.crearMensajeInfo("Clasificacion Actualizada Correctamente.");
	}

	public void actionListenerActualizarCategoria() {
		mParametros.actualizarCategoria(catoSeleccionado);
		listaCategoria = mParametros.findAllCdcCategoria();
		JSFUtil.crearMensajeInfo("Categoria Agregada Correctamente.");
	}

	public int getCategoria() {
		return categoria;
	}

	public void setCategoria(int categoria) {
		this.categoria = categoria;
	}

	public int getClasificacion() {
		return clasificacion;
	}

	public void setClasificacion(int clasificacion) {
		this.clasificacion = clasificacion;
	}

	public List<CdcClasificacion> getListaClasificacion() {
		return listaClasificacion;
	}

	public void setListaClasificacion(List<CdcClasificacion> listaClasificacion) {
		this.listaClasificacion = listaClasificacion;
	}

	public List<CdcCategoria> getListaCategoria() {
		return listaCategoria;
	}

	public void setListaCategoria(List<CdcCategoria> listaCategoria) {
		this.listaCategoria = listaCategoria;
	}

	public String getNombreClasificacion() {
		return nombreClasificacion;
	}

	public void setNombreClasificacion(String nombreClasificacion) {
		this.nombreClasificacion = nombreClasificacion;
	}

	public String getNombreCategoria() {
		return nombreCategoria;
	}

	public void setNombreCategoria(String nombreCategoria) {
		this.nombreCategoria = nombreCategoria;
	}

	public CdcClasificacion getClasiSeleccionado() {
		return clasiSeleccionado;
	}

	public void setClasiSeleccionado(CdcClasificacion clasiSeleccionado) {
		this.clasiSeleccionado = clasiSeleccionado;
	}

	public CdcCategoria getCatoSeleccionado() {
		return catoSeleccionado;
	}

	public void setCatoSeleccionado(CdcCategoria catoSeleccionado) {
		this.catoSeleccionado = catoSeleccionado;
	}

	public int getIdClasiSeleccionado() {
		return idClasiSeleccionado;
	}

	public void setIdClasiSeleccionado(int idClasiSeleccionado) {
		this.idClasiSeleccionado = idClasiSeleccionado;
	}

	public int getIdCatoSeleccionado() {
		return idCatoSeleccionado;
	}

	public void setIdCatoSeleccionado(int idCatoSeleccionado) {
		this.idCatoSeleccionado = idCatoSeleccionado;
	}

}
