package viprisep.controller.guardias;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import minimarketdemo.model.core.entities.CdcEmpresaDetalle;
import minimarketdemo.model.core.entities.CdgCiudad;
import minimarketdemo.model.core.entities.CdgGuardiaCabecera;
import minimarketdemo.model.core.entities.CdgGuardiaDetalle;
import minimarketdemo.model.core.entities.CdgHistorial;
import minimarketdemo.model.core.entities.CdgNivelEducacion;
import minimarketdemo.model.core.entities.CdgProvincia;
import viprisep.controller.util.JSFUtil;
import viprisep.model.guardias.dtos.DTODetalleEducacion;
import viprisep.model.guardias.dtos.DTODetallesEmpresas;
import viprisep.model.guardias.dtos.DTODetallesGuardia;
import viprisep.model.guardias.managers.ManagerGuardias;

@Named
@SessionScoped
public class BeanGuardias implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private ManagerGuardias managerGuardias;

	private List<CdgNivelEducacion> listaTitulos;
	private List<CdgCiudad> listaCiudad;
	private List<CdcEmpresaDetalle> listaEmpresa;
	private List<DTODetallesGuardia> listaDetalleGuardias;
	private List<CdgGuardiaCabecera> listaGuardiacab;
	private List<CdgGuardiaDetalle> listaGuardiaDet;

	/// Para el historial
	private Date fechaAsignacion;
	private Date fechaFinalizacion;
	private Boolean estado;
	private List<CdgHistorial> listaHistorial;
	private CdgHistorial edicionHistorial;
	private int idHistorialSeleccionada;

	private String cedula;
	private String Apellidos;
	private String Nombres;
	private Date fechaNacimiento;
	private String Genero;
	private String celular;
	private Boolean reentrenado;
	private Boolean discapacidad;
	private int porcentajeDiscapacidad;

	private int idTituloSeleccionado;
	private int idUbicacionSeleccionada;
	private int idEmpresaSeleccionada;

	public BeanGuardias() {
		// TODO Auto-generated constructor stub
	}

	@PostConstruct
	public void Inicializar() {
		listaDetalleGuardias = new ArrayList<DTODetallesGuardia>();
		listaTitulos = managerGuardias.findAllCdgNivelEducacion();
		listaGuardiacab = managerGuardias.findAllCdgGuardiaCabecera();
		listaGuardiaDet = managerGuardias.findAllCdgGuardiaDetalle();
		listaHistorial = managerGuardias.findAllHistorial();
		// listaEmpresa = managerGuardias.findAllCdcEmpresaDetalle();
	}

	public void actionListenerGuardarGuardia() {
		managerGuardias.guardiasMultipleEmpresas(listaDetalleGuardias, idEmpresaSeleccionada, cedula);
		listaGuardiacab = managerGuardias.findAllCdgGuardiaCabecera();
		listaDetalleGuardias = new ArrayList<DTODetallesGuardia>();
		listaGuardiaDet = managerGuardias.findAllCdgGuardiaDetalle();
		listaGuardiacab = managerGuardias.findAllCdgGuardiaCabecera();
		JSFUtil.crearMensajeInfo("Guardia Agregado Correctamente.");

		// para el historial
		this.estado = true;
		managerGuardias.insertarHistorial(cedula, fechaAsignacion, fechaFinalizacion, estado);
		listaHistorial = managerGuardias.findAllHistorial();
	}

	public void actionListenerAgregarGuardiacab() {
		listaDetalleGuardias.add(managerGuardias.crearDetalleGuardia(idEmpresaSeleccionada));
		JSFUtil.crearMensajeInfo("Empresa Agregado Correctamente.");
	}

	// metodos para el historial

	public void actionListenerCargarEdicionHistorial(CdgHistorial HistorialAEditar) {

		edicionHistorial = HistorialAEditar;
	}

	public void actionListenerActualizarHistorial() {
		try {
			idHistorialSeleccionada = edicionHistorial.getIdCdgHistorial();
			managerGuardias.editarHistorial(idHistorialSeleccionada, edicionHistorial);
			JSFUtil.crearMensajeInfo("Historial actualizado.");
		} catch (Exception e) {
			JSFUtil.crearMensajeError(e.getMessage());
			e.printStackTrace();
		}
	}

	public String getCedula() {
		return cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	public String getApellidos() {
		return Apellidos;
	}

	public void setApellidos(String apellidos) {
		Apellidos = apellidos;
	}

	public String getNombres() {
		return Nombres;
	}

	public void setNombres(String nombres) {
		Nombres = nombres;
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getGenero() {
		return Genero;
	}

	public void setGenero(String genero) {
		Genero = genero;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public Boolean getReentrenado() {
		return reentrenado;
	}

	public void setReentrenado(Boolean reentrenado) {
		this.reentrenado = reentrenado;
	}

	public Boolean getDiscapacidad() {
		return discapacidad;
	}

	public void setDiscapacidad(Boolean discapacidad) {
		this.discapacidad = discapacidad;
	}

	public int getPorcentajeDiscapacidad() {
		return porcentajeDiscapacidad;
	}

	public void setPorcentajeDiscapacidad(int porcentajeDiscapacidad) {
		this.porcentajeDiscapacidad = porcentajeDiscapacidad;
	}

	public List<DTODetallesGuardia> getListaDetalleGuardias() {
		return listaDetalleGuardias;
	}

	public void setListaDetalleGuardias(List<DTODetallesGuardia> listaDetalleGuardias) {
		this.listaDetalleGuardias = listaDetalleGuardias;
	}

	public List<CdgNivelEducacion> getListaTitulos() {
		return listaTitulos;
	}

	public void setListaTitulos(List<CdgNivelEducacion> listaTitulos) {
		this.listaTitulos = listaTitulos;
	}

	public int getIdTituloSeleccionado() {
		return idTituloSeleccionado;
	}

	public void setIdTituloSeleccionado(int idTituloSeleccionado) {
		this.idTituloSeleccionado = idTituloSeleccionado;
	}

	public int getIdUbicacionSeleccionada() {
		return idUbicacionSeleccionada;
	}

	public void setIdUbicacionSeleccionada(int idUbicacionSeleccionada) {
		this.idUbicacionSeleccionada = idUbicacionSeleccionada;
	}

	public int getIdEmpresaSeleccionada() {
		return idEmpresaSeleccionada;
	}

	public void setIdEmpresaSeleccionada(int idEmpresaSeleccionada) {
		this.idEmpresaSeleccionada = idEmpresaSeleccionada;
	}

	public List<CdcEmpresaDetalle> getListaEmpresa() {
		return listaEmpresa;
	}

	public void setListaEmpresa(List<CdcEmpresaDetalle> listaEmpresa) {
		this.listaEmpresa = listaEmpresa;
	}

	public List<CdgCiudad> getListaCiudad() {
		return listaCiudad;
	}

	public void setListaCiudad(List<CdgCiudad> listaCiudad) {
		this.listaCiudad = listaCiudad;
	}

	public String actionPasarPagina() {
		System.out.println("Holis");
		return "educacion";
	}

	public String actionPasarPagina2() {
		System.out.println("Holis");
		return "clasificacion";
	}

	public String actionPasarPagina3() {
		System.out.println("Holis");
		return "guardia";
	}

	public String actionPasarPagina4() {
		System.out.println("Holis");
		return "asignacion";
	}

	public String actionPasarPagina5() {
		System.out.println("Holis");
		return "historial";
	}

	public String actionPasarPagina6() {
		System.out.println("Holis");
		return "reportes";
	}

	public List<CdgGuardiaCabecera> getListaGuardiacab() {
		return listaGuardiacab;
	}

	public void setListaGuardiacab(List<CdgGuardiaCabecera> listaGuardiacab) {
		this.listaGuardiacab = listaGuardiacab;
	}

	public List<CdgGuardiaDetalle> getListaGuardiaDet() {
		return listaGuardiaDet;
	}

	public void setListaGuardiaDet(List<CdgGuardiaDetalle> listaGuardiaDet) {
		this.listaGuardiaDet = listaGuardiaDet;
	}

	// PAra el historial
	public Date getFechaAsignacion() {
		return fechaAsignacion;
	}

	public void setFechaAsignacion(Date fechaAsignacion) {
		this.fechaAsignacion = fechaAsignacion;
	}

	public Date getFechaFinalizacion() {
		return fechaFinalizacion;
	}

	public void setFechaFinalizacion(Date fechaFinalizacion) {
		this.fechaFinalizacion = fechaFinalizacion;
	}

	public Boolean getEstado() {
		return estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	public List<CdgHistorial> getListaHistorial() {
		return listaHistorial;
	}

	public void setListaHistorial(List<CdgHistorial> listaHistorial) {
		this.listaHistorial = listaHistorial;
	}

	public CdgHistorial getEdicionHistorial() {
		return edicionHistorial;
	}

	public void setEdicionHistorial(CdgHistorial edicionHistorial) {
		this.edicionHistorial = edicionHistorial;
	}

	public int getIdHistorialSeleccionada() {
		return idHistorialSeleccionada;
	}

	public void setIdHistorialSeleccionada(int idHistorialSeleccionada) {
		this.idHistorialSeleccionada = idHistorialSeleccionada;
	}

}
